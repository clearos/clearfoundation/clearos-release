# clearos-release

For the Centos and RedHat version numbers, check the upstream centos-release package and do an "rpm -qp --provides" on it.

- centos-release is base_release_version. Please see https://gitlab.com/clearos/clearfoundation/clearos-release/-/issues/14.
- centos-release-upstream is defined by upstream_rel.
- clearos_release_version is the main part of the string which goes into /etc/clearos-release. It does not need to mirror clearos_rel.
- clearos_rel with base_release_version make up the clearos-release package number. It needs to be bumped for every build.
- clearos-release will mirror the package name and comes from version (base_release_version) and release(clearos_rel).
- clearos-release(upstream) is defined by %{upstream_rel}. Historically this is just the major.minor version and echoes upstream_rel.
- clearos-release(x86-64) and config(clearos-release) are created by the rpm packaging and reflect clearos-release.
- epel-release generally has not had a version set, but you could possibly use the version in the upstream epel-release.
- redhat-release comes from upstream_rel_long.
- system-release comes from upstream_rel_long.
- centos_rel should be the bit after the 7- in the upstream centos-release. If the upstream centos-release provides `7-8.2003.0.el7.centos`, this could be set to `7-8.2003` or `7-8.2003.0.el7.centos`.
- system-release(releasever) is defined by base_release_version.

- full_release_version should probably be set the same as base_release_version.
- dist_release_version is not currently used.
